/**
 * @file main.c
 * @author Misa
 * @date 2022-03-31
 * @brief Day 7: The Treachery of Whales (https://adventofcode.com/2021/day/7)
 * 
 *  Requirements:
 *  - crab submarines can only move horizontally
 *  - input: list of the horizontal position of each crab
 *  - Each change of 1 step in horizontal position of a single crab costs 1 fuel.
 *  - Determine the horizontal position that the crabs can align to using the least fuel possible. 
 *  - How much fuel must they spend to align to that position?
 */

#include <common.h>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <errno.h>

#define LINEBUFFER_MAX_LENGTH		5000

#define CRABS_MAX_NUMBER			2000
typedef struct {
	uint32_t pos[CRABS_MAX_NUMBER];
	uint32_t count;
	uint32_t max_pos;
} crabs_t;

crabs_t crabs = {
	.count = 0,
	.max_pos = 0,
	.pos = {0}
};

Std_ReturnType crabs_addnew(uint32_t pos) {
	if (crabs.count >= CRABS_MAX_NUMBER) {
		printf("ERR: CRABS_MAX_NUMBER reached\n");
		return E_NOT_OK;
	}

	crabs.pos[crabs.count] = pos;

	if (pos > crabs.max_pos) {
		crabs.max_pos = pos;
	}

	crabs.count++;
	return E_OK;
}

static inline uint32_t calc_sum_of_numbers(uint32_t number) {
	uint32_t res = 0;
	res = (number*(number+1))/2;

	return res;
}

uint32_t crabs_get_fuel_by_pos(uint32_t pos) {
	uint32_t ret_fuel = 0;

	for (uint32_t i = 0; i < crabs.count; i++) {
		uint32_t curr_fuel = 0;
		curr_fuel = absdiff(crabs.pos[i], pos);
		ret_fuel += calc_sum_of_numbers(curr_fuel);

	}

	//printf("Pos %u.: %u fuel\n", pos, ret_fuel);

	return ret_fuel;
}

void crabs_print(void) {
	printf("Crabs: count %u max_pos: %u\n", crabs.count, crabs.max_pos);
}

void main_parse_line(char *line, int length, int line_number) {
	char *token = NULL;	// For strtok usage
	char *end = NULL; // For strtol usage

	//printf("Line length: %d\n", length);
	//printf("Line %s\n", line);

	token = strtok(line, ",");
	while (token != NULL) {
		uint32_t hpos = 0;
		hpos = strtol(token, &end, 10);
		crabs_addnew(hpos);
		token = strtok(NULL, ",");
	}
}

int main(int argc, char **argv) {
    char *filename = "inputs/2021_07_input.txt";

    char linebuffer[LINEBUFFER_MAX_LENGTH];
    int line_number = 0;

    FILE *fp = fopen(filename, "r");

    if (fp == NULL)
    {
        printf("Error: could not open file %s", filename);
        return 1;
    }

    while (fgets(linebuffer, sizeof(linebuffer), fp)) {
    	main_parse_line(linebuffer, strlen(linebuffer), line_number);
		line_number++;
    }

	// ********************************************************************************
	// START of Task specific Code
	// ********************************************************************************

	crabs_print();
	uint32_t min_fuel = UINT32_MAX;
	uint32_t res_pos = 0;

	for (uint32_t i = 0; i < crabs.max_pos; i++) {
		uint32_t current_fuel = crabs_get_fuel_by_pos(i);
		if (current_fuel <= min_fuel) {
			res_pos = i;
			min_fuel = current_fuel;
		}
	}
	
	printf("min_fuel: %u, min pos: %u\n", min_fuel, res_pos);
	// ********************************************************************************
	// END of Task specific Code
	// ********************************************************************************

    // close the file
    fclose(fp);
	return 0;
}


