/**
 * @file main.c
 * @author Misa
 * @date 2022-04-09
 * @brief Day 8: Seven Segment Search (https://adventofcode.com/2021/day/8)
 * 
 *  Requirements:
 *  - Each entry consists of ten unique signal patterns, a | delimiter, 
 * 	  and finally the four digit output value
 *  - the digits 1, 4, 7, and 8 each use a unique number of segments
 * 
 *  Part 1:
 *  - In the output values, how many times do digits 1, 4, 7, or 8 appear?
 */

#include <common.h>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <errno.h>

#define LINEBUFFER_MAX_LENGTH		300

#define SIGNAL_PATTERN_COUNT 		10
#define SIGNAL_PATTERN_LENGTH 		(8+1) // String so end char also should be counted

#define OUTPUT_VALUE_COUNT 			4
#define OUTPUT_VALUE_LENGTH			(8+1) // String so end char also should be counted

#define SEGMENT_0	"abcefg"
#define SEGMENT_1	"cf"
#define SEGMENT_2	"acdeg"
#define SEGMENT_3	"abdfg"
#define SEGMENT_4	"bcdf"
#define SEGMENT_5	"abdfg"
#define SEGMENT_6	"abdefg"
#define SEGMENT_7	"acf"
#define SEGMENT_8	"abcdefg"
#define SEGMENT_9	"abcdfg"

typedef struct {
	char segment[8];
} segment_t;

typedef struct {
	char signal_patterns[SIGNAL_PATTERN_COUNT][SIGNAL_PATTERN_LENGTH];
	char output_values[OUTPUT_VALUE_COUNT][OUTPUT_VALUE_LENGTH];
} signal_entry_t;

#define SIGNAL_ENTRIES_MAX	200
signal_entry_t signal_entries[SIGNAL_ENTRIES_MAX];
uint32_t signal_entries_count = 0;

void signals_print_entry(uint32_t entry_idx) {
	printf("#%03u:\n", entry_idx);
	printf("\tsignal patterns: ");
	uint8_t i;
	for (i = 0; i < SIGNAL_PATTERN_COUNT; i++) {
		printf("%s(%lld) ", signal_entries[entry_idx].signal_patterns[i], strlen(signal_entries[entry_idx].signal_patterns[i]));
	}
	printf("\n");
	printf("\toutput values: ");
	for (i = 0; i < OUTPUT_VALUE_COUNT; i++) {
		printf("%s(%lld) ", signal_entries[entry_idx].output_values[i], strlen(signal_entries[entry_idx].output_values[i]));
	}
	printf("\n--------------------------------------\n");
}


void signalentry_parsing_patterns(char *pattern_string) {
	char *token = NULL;	// For strtok usage
	uint32_t idx = 0;

	token = strtok(pattern_string, " ");

	while (token != NULL && idx < SIGNAL_PATTERN_COUNT) {
		strcpy(signal_entries[signal_entries_count].signal_patterns[idx], token);
		idx++;
		token = strtok(NULL, " ");
	}
}

void signal_parsing_output(char *output_values_string) {
	char *token = NULL;	// For strtok usage
	uint32_t idx = 0;

	token = strtok(output_values_string, " ");

	while (token != NULL && idx < OUTPUT_VALUE_COUNT) {
		strcpy(signal_entries[signal_entries_count].output_values[idx], token);
		// Remove line end if this is the last char of an token
		if (signal_entries[signal_entries_count].output_values[idx][strlen(token)-1] == '\n') {
			signal_entries[signal_entries_count].output_values[idx][strlen(token)-1] = 0;
		}
		idx++;
		token = strtok(NULL, " ");
	}
}

uint32_t signal_count_of_segmentlength_in_output(char *segment) {
	uint32_t ret = 0;

	for (uint32_t i = 0; i < signal_entries_count; i++) {
		for (uint32_t j = 0; j < OUTPUT_VALUE_COUNT; j++) {
			if (strlen(signal_entries[i].output_values[j]) == strlen(segment)) {
				ret++;
			}
		}
	}
	
	return ret;
}

void print_segment(char *segment) {
	if (sizeof(segment) < 7) {
		printf("FATAL ERROR\n");
		return;
	}

	printf("\n");
	printf("1| %c%c%c%c \n", segment[0],segment[0],segment[0],segment[0]);
	printf("2|%c    %c\n", segment[1], segment[2]);
	printf("3|%c    %c\n", segment[1], segment[2]);
	printf("4| %c%c%c%c \n", segment[3], segment[3], segment[3], segment[3]);
	printf("5|%c    %c\n", segment[4], segment[5]);
	printf("6|%c    %c\n", segment[4], segment[5]);
	printf("7| %c%c%c%c \n", segment[6], segment[6], segment[6], segment[6]);
	printf("\n");
}

void signals_find_segment(uint32_t entry_idx) {
	uint8_t i;
	
	char number1[8];
	char number2[8];
	char number3[8];
	char number4[8];
	char number5[8];
	char number6[8];
	char number7[8];
	char number8[8];
	char number9[8];

	char number069[3][8];
	uint8_t number69_idx = 0;

	for (i = 0; i < SIGNAL_PATTERN_COUNT; i++) {
		if (strlen(SEGMENT_1) == strlen(signal_entries[entry_idx].signal_patterns[i])) {
			strcpy(number1, signal_entries[entry_idx].signal_patterns[i]);
		}

		if (strlen(SEGMENT_6) == strlen(signal_entries[entry_idx].signal_patterns[i])) {
			strcpy(number069[number69_idx++], signal_entries[entry_idx].signal_patterns[i]);
		}

		if (strlen(SEGMENT_4) == strlen(signal_entries[entry_idx].signal_patterns[i])) {
			strcpy(number4, signal_entries[entry_idx].signal_patterns[i]);
		}

		if (strlen(SEGMENT_7) == strlen(signal_entries[entry_idx].signal_patterns[i])) {
			strcpy(number7, signal_entries[entry_idx].signal_patterns[i]);
		}

		if (strlen(SEGMENT_8) == strlen(signal_entries[entry_idx].signal_patterns[i])) {
			strcpy(number8, signal_entries[entry_idx].signal_patterns[i]);
		}
	}

	char segment[8] = {0,0,0,0,0,0,0,0};

	if ((strlen(number7) != 0) && (strlen(number1) != 0)) {
		for (i=0; i < strlen(number7); i++) {
			if (number7[i] != number1[0] && number7[i] != number1[1]) {
				segment[0] = number7[i];
			}
		}
	}

		if (strchr(number069[0], number1[0]) == NULL && strchr(number069[0], number1[1]) == NULL) {
			strcpy(number6, number069[0]);
		}

		if (strchr(number069[1], number1[0]) == NULL && strchr(number069[1], number1[1]) == NULL) {
			strcpy(number6, number069[1]);
		}

		if (strchr(number069[2], number1[0]) == NULL && strchr(number069[2], number1[1]) == NULL) {
			strcpy(number6, number069[2]);
		}
	
	printf("#%03u| 1:%s, 4:%s, 6:%s 7:%s, 8:%s, 9:%s\n", entry_idx, 
				number1, number4, number6, number7, number8, number9
	);

	print_segment(segment);
}

void main_parse_line(char *line, int length, int line_number) {
	char *token_line = NULL;	// For strtok usage
	//char *end = NULL; // For strtol usage

	char signal_patterns[LINEBUFFER_MAX_LENGTH];
	char output_values[LINEBUFFER_MAX_LENGTH];

	//printf("Line length: %d\n", length);
	//printf("Line %s\n", line);
	
	// Get First part of the Line
	token_line = strtok(line, "|");
	strcpy(signal_patterns, token_line);
//	printf("#%u: %s|", line_number, signal_patterns);

	
	
	// Get First part of the Line
	token_line = strtok(NULL, "|");
	strcpy(output_values, token_line);
//	printf("%s", output_values);

	signalentry_parsing_patterns(signal_patterns);
	signal_parsing_output(output_values);

	//signals_print_entry(signal_entries_count);

	signal_entries_count++;
}

int main(int argc, char **argv) {
    char *filename = "inputs/2021_08_input_test.txt";

    char linebuffer[LINEBUFFER_MAX_LENGTH];
    int line_number = 0;

    FILE *fp = fopen(filename, "r");

    if (fp == NULL)
    {
        printf("Error: could not open file %s", filename);
        return 1;
    }

    while (fgets(linebuffer, sizeof(linebuffer), fp)) {
    	main_parse_line(linebuffer, strlen(linebuffer), line_number);
		line_number++;
    }

	// ********************************************************************************
	// START of Task specific Code
	// ********************************************************************************
	uint32_t part1_result = 0;
	part1_result += signal_count_of_segmentlength_in_output(SEGMENT_1);
	part1_result += signal_count_of_segmentlength_in_output(SEGMENT_4);
	part1_result += signal_count_of_segmentlength_in_output(SEGMENT_7);
	part1_result += signal_count_of_segmentlength_in_output(SEGMENT_8);

	printf("Part1 result: %u\n", part1_result);

	// be cfbegad cbdgef fgaecd cgeb fdcge agebfd fecdb fabcd edb | fdgacbe cefdb cefbgd gcbe: 8394
#if 0
	for (size_t i = 0; i < signal_entries_count; i++) {
		signals_find_segment(i);	
	}
#else
	signals_find_segment(0);
#endif

	// ********************************************************************************
	// END of Task specific Code
	// ********************************************************************************

    // close the file
    fclose(fp);
	return 0;
}


