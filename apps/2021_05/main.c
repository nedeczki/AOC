#include <common.h>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <errno.h>

#define LINEBUFFER_MAX_LENGTH		512

#define DIAGRAM_SIZE				1000
#define SEGMENTS_MAX_SIZE			500
typedef struct {
	uint16_t x1;
	uint16_t y1;
	uint16_t x2;
	uint16_t y2;
} segment_t;

struct {
	segment_t segments[SEGMENTS_MAX_SIZE];
	uint16_t segment_count;
	uint16_t diagram[DIAGRAM_SIZE][DIAGRAM_SIZE];
} ht_vent;

void segment_print( uint16_t id, segment_t segment) {
	printf("#%03d: start: %d,%d end: %d,%d\n", 
		id,
		segment.x1,
		segment.y1,
		segment.x2,
		segment.y2
	);
}

void diagram_print(void) {
	for (uint16_t y = 0; y < DIAGRAM_SIZE; y++) {
		for (uint16_t x = 0; x < DIAGRAM_SIZE; x++) {
			printf(" %d ", ht_vent.diagram[x][y]);
		}
		printf("\n");
	}
	printf("\n");
}

void diagram_draw_line(segment_t segment) {
	//printf("Drawing segment:  %d,%d -> %d,%d\n", segment.x1, segment.y1, segment.x2, segment.y2);

	// Draw vertical line
	if (segment.x1 == segment.x2) {
		//printf("Draw vertical line: %d,%d -> %d,%d\n", segment.x1, segment.y1, segment.x2, segment.y2);
		if (segment.y1 >= segment.y2) {
			for (uint16_t y = segment.y2; y <= segment.y1; y++) {
				ht_vent.diagram[segment.x1][y]++;				
			}
		} else {
			for (uint16_t y = segment.y1; y <= segment.y2; y++) {
				ht_vent.diagram[segment.x1][y]++;				
			}
		}
		//diagram_print();
	}
	
	// Draw horizontal line
	if (segment.y1 == segment.y2) {
		//printf("Draw horizontal line: %d,%d -> %d,%d\n", segment.x1, segment.y1, segment.x2, segment.y2);
		if (segment.x1 >= segment.x2) {
			for (uint16_t x = segment.x2; x <= segment.x1; x++) {
				ht_vent.diagram[x][segment.y1]++;				
			}
		} else {
			for (uint16_t x = segment.x1; x <= segment.x2; x++) {
				ht_vent.diagram[x][segment.y1]++;				
			}
		}

		//diagram_print();
	}

	// Draw diagonal line: difference between the 
	if (absdiff(segment.x1, segment.x2) == absdiff(segment.y1, segment.y2)) {
		//printf("Draw diagonal line: %d,%d -> %d,%d\n", segment.x1, segment.y1, segment.x2, segment.y2);
		// 8,0 -> 0,8

		if (segment.x1 > segment.x2 && segment.y1 < segment.y2) {
			uint16_t y = segment.y2;
			for (uint16_t x = segment.x2; x <= segment.x1 ; x++) {
				ht_vent.diagram[x][y]++;
				y--;
			}
		}

		if (segment.x1 > segment.x2 && segment.y1 > segment.y2) {
			uint16_t y = segment.y2;
			for (uint16_t x = segment.x2; x <= segment.x1 ; x++) {
				ht_vent.diagram[x][y]++;
				y++;
			}
		}

		if (segment.x1 < segment.x2 && segment.y1 < segment.y2) {
			uint16_t y = segment.y1;
			for (uint16_t x = segment.x1; x <= segment.x2 ; x++) {
				ht_vent.diagram[x][y]++;
				y++;
			}
		}

		if (segment.x1 < segment.x2 && segment.y1 > segment.y2) {
			uint16_t y = segment.y1;
			for (uint16_t x = segment.x1; x <= segment.x2 ; x++) {
				ht_vent.diagram[x][y]++;
				y--;
			}
		}

		//diagram_print();
	}

	//printf("-------------------------------------------------\n");
}

void diagram_draw_lines(void) {
	for (uint16_t i = 0; i < ht_vent.segment_count; i++) {
		diagram_draw_line(ht_vent.segments[i]);
	}

	diagram_print();
}

uint32_t diagram_calc_overlaps(void) {
	uint32_t overlaps = 0;

	for (uint16_t y = 0; y < DIAGRAM_SIZE; y++) {
		for (uint16_t x = 0; x < DIAGRAM_SIZE; x++) {
			if (ht_vent.diagram[x][y] >= 2) {
				overlaps++;
			}
		}
	}

	return overlaps;
}

void process_line(char *line, int length, int line_number) {
	char *token = NULL;	// For strtok usage
	char *end = NULL; // For strtol usage
	char segment_start[20];
	char segment_end[20];

	// printf("line #%d: %s", line_number, line);

	token = strtok(line, " -> ");
	
	if (token != NULL) {
		strcpy(segment_start, token);
    	token = strtok(NULL, " -> ");

		if (token != NULL) {
			strcpy(segment_end, token);	
		}
	}

	token = strtok(segment_start, ",");
	if (token != NULL) {
		ht_vent.segments[ht_vent.segment_count].x1 = strtol(token, &end, 10);
		token = strtok(NULL, ",");
		if (token != NULL) {
			ht_vent.segments[ht_vent.segment_count].y1 = strtol(token, &end, 10);
		}
	}

	token = strtok(segment_end, ",");
	if (token != NULL) {
		ht_vent.segments[ht_vent.segment_count].x2 = strtol(token, &end, 10);
		token = strtok(NULL, ",");
		if (token != NULL) {
			ht_vent.segments[ht_vent.segment_count].y2 = strtol(token, &end, 10);
		}
	}
	segment_print(ht_vent.segment_count, ht_vent.segments[ht_vent.segment_count]);
	ht_vent.segment_count++;
}

int main(int argc, char **argv) {
    char *filename = "inputs/2021_05_input.txt";

    char linebuffer[LINEBUFFER_MAX_LENGTH];
    int line_number = 0;

    FILE *fp = fopen(filename, "r");

    if (fp == NULL)
    {
        printf("Error: could not open file %s", filename);
        return 1;
    }

    while (fgets(linebuffer, sizeof(linebuffer), fp)) {
    	process_line(linebuffer, strlen(linebuffer), line_number);
		line_number++;
    }

	diagram_draw_lines();

	uint32_t overlaps = diagram_calc_overlaps();
	printf("Number of overlaps: %u\n", overlaps);

    // close the file
    fclose(fp);
	return 0;
}


