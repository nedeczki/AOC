/**
 * @file main.c
 * @author Misa
 * @brief Day 6: Lanternfish (https://adventofcode.com/2021/day/6)
 * @date 2022-03-31
 * 
 * Requirements:
 *  - each lanternfish creates a new lanternfish once every 7 days
 *  - each fish as a single number that represents the number of days 
 * 	  until it creates a new lanternfish.
 *  - New lanterfish capable of producing more lanternfish: two more days for its first cycle.
 */

#include <common.h>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <errno.h>

#define LINEBUFFER_MAX_LENGTH		1000

#define LIFECYCLE_NOTEXISTING	0
#define LIFECYCLE_NEWBORN		1
#define LIFECYCLE_NORMAL		2
typedef uint8_t lifecycle_t;

#define PRODCYCLE_NORMAL	6
#define PRODCYCLE_NEWBORN	(PRODCYCLE_NORMAL + 2)

typedef struct {
	lifecycle_t lifecycle;
	uint8_t prodcycle;
} lanternfish_t;

#define LANTERFISHES_MAX_NUMBER		1000000UL
lanternfish_t lanternfishes[LANTERFISHES_MAX_NUMBER];
uint32_t lanternfish_count = 0;

void lanternfish_add(lifecycle_t lifecycle, uint8_t prodcycle) {
	lanternfishes[lanternfish_count].lifecycle = lifecycle;
	lanternfishes[lanternfish_count].prodcycle = prodcycle;

	lanternfish_count++;

	if (lanternfish_count == LANTERFISHES_MAX_NUMBER) {
		printf("ERROR: Maximum number of lanterfishes reached: %ld\n", LANTERFISHES_MAX_NUMBER);
	}
}

void lanterfishes_print(void) {
	for (uint32_t i = 0; i < lanternfish_count; i++) {
		if (i == lanternfish_count - 1) {
			printf("%d", lanternfishes[i].prodcycle);
		} else {
			printf("%d,", lanternfishes[i].prodcycle);
		}
	}
}

void lanterfish_aging_all(void) {
	// Lanternfish count have to be stored before aging to not aging the newborns at first day
	uint32_t lfish_count_before_aging = lanternfish_count;

	for (uint32_t i = 0; i < lfish_count_before_aging; i++) {
		if (lanternfishes[i].lifecycle == LIFECYCLE_NEWBORN) {
			lanternfishes[i].lifecycle = LIFECYCLE_NORMAL;
		}

		if (lanternfishes[i].prodcycle == 0) {
			lanternfishes[i].prodcycle = PRODCYCLE_NORMAL;

			lanternfish_add(LIFECYCLE_NEWBORN, PRODCYCLE_NEWBORN);
		} else {
			lanternfishes[i].prodcycle--;
		}
	}
}

void lanterfish_simalute_days(uint8_t days) {
	for (uint8_t day = 1; day <= days; day++) {

		lanterfish_aging_all();
		//printf("Day %d: ", day);
		//lanterfishes_print();
		//printf("\n");
	}
	
}

void main_parse_line(char *line, int length, int line_number) {
	char *token = NULL;	// For strtok usage
	char *end = NULL; // For strtol usage

	token = strtok(line, ",");
	while (token != NULL) {
		uint8_t prodcycle = 0;
		prodcycle = strtol(token, &end, 10);
		lanternfish_add(LIFECYCLE_NORMAL, prodcycle);
		token = strtok(NULL, ",");
	}

	printf("Initial state: ");
	lanterfishes_print();
	printf("\n");
}

int main(int argc, char **argv) {
    char *filename = "inputs/2021_06_input.txt";

    char linebuffer[LINEBUFFER_MAX_LENGTH];
    int line_number = 0;

    FILE *fp = fopen(filename, "r");

    if (fp == NULL)
    {
        printf("Error: could not open file %s", filename);
        return 1;
    }

    while (fgets(linebuffer, sizeof(linebuffer), fp)) {
    	main_parse_line(linebuffer, strlen(linebuffer), line_number);
		line_number++;
    }

	// ********************************************************************************
	// START of Task specific Code
	// ********************************************************************************

	lanterfish_simalute_days(80);
	printf("Lanterfish count: %d\n", lanternfish_count);

	// ********************************************************************************
	// END of Task specific Code
	// ********************************************************************************

    // close the file
    fclose(fp);
	return 0;
}


