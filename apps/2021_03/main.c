#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <errno.h>
#include <common/Std_Types.h>
#include <binnum/binnum.h>

#define DIAGREP_POSITION_MAX	20

typedef struct {
	int type;
	uint16_t bit_0_count;
	uint16_t bit_1_count;
} diagrep_position_t;

#define DIAGREP_DATA_MAX_LENGTH 1000
typedef struct {
	uint16_t value;
	uint8_t isvalid;
} diagrep_data_t;

#define DIAGREP_DATA_INVALID_VALUE 0xffff;

typedef struct {
	uint32_t gamma_rate;
	uint32_t epsilon_rate;

	diagrep_data_t data[DIAGREP_DATA_MAX_LENGTH];
	uint16_t valid_data_count;
} diagrep_t;

uint8_t diagrep_get_most_common_bit_from_valid(uint8_t pos);

diagrep_t diagrep = {.gamma_rate=0, .epsilon_rate=0, .valid_data_count=0};

void diagrep_set_data_to_valid(uint16_t new_valid_data_count) {
	diagrep.valid_data_count = new_valid_data_count;

	for (int i=0; i<diagrep.valid_data_count; i++) {
		diagrep.data[i].isvalid = 1;
	}
}

void process_line(char *line, int length, int line_number) {
	char *end;

	//printf("line (%d, %d) %s\n", line_number, length, line);
	diagrep.data[line_number].value = (uint16_t)strtol(line, &end, 2);
	diagrep.data[line_number].isvalid = 1;
	diagrep.valid_data_count++;
}

void calc_gamma_and_epsilon_rate(int pos_count) {
	for (int i=pos_count; i >= 0; i--) {
		if (diagrep_get_most_common_bit_from_valid(i) == 0) {
			diagrep.gamma_rate |= 1 << i;
		} else {
			diagrep.epsilon_rate |= 1 << i;
		};
	}

	printf("position_count:%d, gamma_rate: %d, epsilon_rate: %d\n", pos_count, diagrep.gamma_rate, diagrep.epsilon_rate);
}

uint8_t diagrep_get_most_common_bit_from_valid(uint8_t pos) {
	uint16_t bit0_count = 0;
	uint16_t bit1_count = 0;

	for (int i=0; i < DIAGREP_DATA_MAX_LENGTH; i++) {
		if (diagrep.data[i].isvalid) {
			if (binnum_get_bit_u16(diagrep.data[i].value, pos) == 0) {
				bit0_count++;
			} else {
				bit1_count++;
			}
		}
	}

	// printf("most_common_bit debug: 0:%d, 1:%d\n", bit0_count, bit1_count);

	if (bit1_count >= bit0_count)
		return 1;
	else
		return 0;
}

uint8_t diagrep_get_least_common_bit_from_valid(uint8_t pos) {
	uint16_t bit0_count = 0;
	uint16_t bit1_count = 0;

	for (int i=0; i < DIAGREP_DATA_MAX_LENGTH; i++) {
		if (diagrep.data[i].isvalid) {
			if (binnum_get_bit_u16(diagrep.data[i].value, pos) == 0) {
				bit0_count++;
			} else {
				bit1_count++;
			}
		}
	}

	// printf("least_common_bit debug: 0:%d, 1:%d\n", bit0_count, bit1_count);

	if (bit1_count >= bit0_count)
		return 0;
	else
		return 1;
}

void diagrep_invalidate_data_for_co2sr(uint8_t pos) {
	uint8_t least_common_bit = diagrep_get_least_common_bit_from_valid(pos);
	// printf("%d: lcb:%d\n", pos, least_common_bit);

	for (int i=0; i < DIAGREP_DATA_MAX_LENGTH; i++) {
		if (diagrep.data[i].isvalid) {
			if (binnum_get_bit_u16(diagrep.data[i].value, pos) != least_common_bit) {
				diagrep.data[i].isvalid=0;
				// printf("\tdeleted:");
				// binnum_print_u16_number(diagrep.data[i].value, 11);
				diagrep.valid_data_count--;
			}
		}
	}
	// printf("invalid_data_count: %d\n", diagrep.valid_data_count);
}

void diagrep_invalidate_data_for_ogr(uint8_t pos) {
	uint8_t most_common_bit = diagrep_get_most_common_bit_from_valid(pos);
	// printf("%d: mcb:%d\n", pos, most_common_bit);

	for (int i=0; i < DIAGREP_DATA_MAX_LENGTH; i++) {
		if (diagrep.data[i].isvalid) {
			if (binnum_get_bit_u16(diagrep.data[i].value, pos) != most_common_bit) {
				diagrep.data[i].isvalid=0;
				// printf("\tdeleted:");
				// print_u16_number(diagrep.data[i].value, 11);
				diagrep.valid_data_count--;				
			}
		}
	}
	// printf("invalid_data_count: %d\n", diagrep.valid_data_count);
}

Std_ReturnType get_first_valid_value(uint16_t *fv_value) {
	for (int i=0; i < DIAGREP_DATA_MAX_LENGTH; i++) {
		if (diagrep.data[i].isvalid) {
			*fv_value = diagrep.data[i].value;
			return E_OK;
		}
	}

	*fv_value = DIAGREP_DATA_INVALID_VALUE;
	return E_NOT_OK;
}

uint16_t calc_oxygen_generator_rating(uint8_t length) {
	uint16_t ogr = 0;
	Std_ReturnType ret;

	for (int i=length; i >= 0; i--) {
		diagrep_invalidate_data_for_ogr(i);

		if (diagrep.valid_data_count == 1) {
			// search for valid data
			ret = get_first_valid_value(&ogr);
			if (ret == E_OK) {
				printf("OGR Rating found: %u\n", ogr);
			} else {
				printf("OGR Rating error!\n");
			}

			return ogr;
		}
	}

	return 0;
}

uint32_t calc_CO2_scrubber_rating(uint8_t length) {
	uint16_t co2sr = 0;
	Std_ReturnType ret;

	for (int i=length; i >= 0; i--) {
		diagrep_invalidate_data_for_co2sr(i);

		if (diagrep.valid_data_count == 1) {
			// search for valid data
			ret = get_first_valid_value(&co2sr);
			if (ret == E_OK) {
				printf("CO2S Rating found: %u\n", co2sr);
			} else {
				printf("CO2S Rating ERROR!\n");
			}
			
			return co2sr;
		}
	} 

	return 0;
}

int main(int argc, char **argv) {
    char *filename = "inputs/2021_03_input.txt";
    // reading line by line, max 256 bytes
    const unsigned MAX_LENGTH = 256;
    char buffer[MAX_LENGTH];
    int line_number = 0;
    // Task specific variables
    int position_length = 0;

    FILE *fp = fopen(filename, "r");

    if (fp == NULL)
    {
        printf("Error: could not open file %s", filename);
        return 1;
    }

    while (fgets(buffer, MAX_LENGTH, fp)) {
    	process_line(buffer, strlen(buffer), line_number);
    	line_number++;
		// -2:= strlen gives back size+1 (end char) + bit positions starts from 0.
    	position_length = strlen(buffer)-2; 
    }

    calc_gamma_and_epsilon_rate(position_length);

    uint16_t current_valid_data_count = diagrep.valid_data_count;
	
	printf("initial valid data count: %d\n", diagrep.valid_data_count);
    uint32_t ogr = calc_oxygen_generator_rating(position_length);
    diagrep_set_data_to_valid(current_valid_data_count);
    uint32_t co2sr = calc_CO2_scrubber_rating(position_length);

    printf("power consumption of the submarine: %d\n", (diagrep.gamma_rate * diagrep.epsilon_rate));
    printf("life support rating of the submarine: %d\n", ogr*co2sr);
    // close the file
    fclose(fp);
	return 0;
}


