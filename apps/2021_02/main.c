#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>

#define MAX_COMMANDS 1500

typedef enum {
	forward = 0,
	down = 1,
	up = 2,
	unknown = 3
} direction_en;

typedef struct {
	int horizontal_position;
	int depth;
	int aim;
} submarine_t;

submarine_t submarine = {
		.horizontal_position = 0,
		.depth = 0,
		.aim = 0
};


void process_command(char *cmd, char *xunit) {
	char *end;

	if (strcmp(cmd, "forward") == 0) {
		submarine.horizontal_position += strtol(xunit, &end, 10);
		submarine.depth += submarine.aim * strtol(xunit, &end, 10);
	}

	if (strcmp(cmd, "down") == 0) {
		submarine.aim += strtol(xunit, &end, 10);
	}

	if (strcmp(cmd, "up") == 0) {
		submarine.aim -= strtol(xunit, &end, 10);
	}
}

int main(int argc, char **argv) {
    char *filename = "inputs/2021_02_input.txt";
    // reading line by line, max 256 bytes
    const unsigned MAX_LENGTH = 256;
    char buffer[MAX_LENGTH];
    int line_number = 0;
    // Task specific variables
    char *token;
    char cmd[30];
    char xunit[30];

    memset(cmd, 0, sizeof(cmd));
    memset(xunit, 0, sizeof(xunit));

    FILE *fp = fopen(filename, "r");

    if (fp == NULL)
    {
        printf("Error: could not open file %s", filename);
        return 1;
    }

    while (fgets(buffer, MAX_LENGTH, fp)) {
    	line_number++;

    	token = strtok(buffer, " ");

    	if (token != NULL) {
    		strcpy(cmd, token);
    		token = strtok(NULL, " ");

    		if (token != NULL) {
    			strcpy(xunit, token);
    		}
    	}

    	if (cmd[0] != 0 && xunit[0] != 0) {
    		process_command(cmd, xunit);
    	}
/*

*/
    }

    printf("RESULT 1: horizontal_pos: %d, depth: %d multiplier: %d\n", submarine.horizontal_position, submarine.depth, submarine.horizontal_position * submarine.depth);
    printf("RESULT 2: %d\n", 2);
    // close the file
    fclose(fp);
	return 0;
}


