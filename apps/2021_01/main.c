#include <stdio.h>
#include <stdlib.h>
#include <errno.h>

#define MAX_MEASURMENTS 2000

struct measurements {
	int data[MAX_MEASURMENTS];
	int count;
} measurements;

int main(int argc, char **argv) {
    char *filename = "inputs/2021_01_input.txt";
    FILE *fp = fopen(filename, "r");

    if (fp == NULL)
    {
        printf("Error: could not open file %s", filename);
        return 1;
    }

    int increased_count = 0;
    int window_increased_count = 0;
    long last_value = 0;

    // reading line by line, max 256 bytes
    const unsigned MAX_LENGTH = 256;
    char buffer[MAX_LENGTH];
    int line_number = 0;
    while (fgets(buffer, MAX_LENGTH, fp)) {
    	errno = 0;
    	char *end;
    	const long measurement = strtol(buffer, &end, 10);
    	measurements.data[line_number] = measurement;
    	measurements.count = line_number;



    	if (line_number == 0) {
    		increased_count = 0;
    	} else {
    		if (last_value < measurement) {
    			increased_count++;
    		}
    	}

    	last_value = measurement;

    	//printf("%d.: %ld (increased: %d)\n", line_number, measurement, increased_count);
    	line_number++;
    }

    printf("measurement count: %d\n", measurements.count);

    long last_window_value = 0;
    int max_window_measurements = measurements.count - (measurements.count % 3);
    for (int i=0; i < max_window_measurements; i++) {
    	long current_window_value = measurements.data[i] + measurements.data[i+1] + measurements.data[i+2];

    	if (last_window_value != 0) {
    		if (current_window_value > last_window_value) {
    			window_increased_count++;
    		}
    	}

    	last_window_value = current_window_value;
    }

    printf("RESULT 1: %d\n", increased_count);
    printf("RESULT 2: %d\n", window_increased_count);
    // close the file
    fclose(fp);
	return 0;
}


