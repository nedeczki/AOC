#include <common/Std_Types.h>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <errno.h>

#define LINEBUFFER_MAX_LENGTH		512

#define BINGO_RANDOMNUMBER_MAXSIZE	100
#define BINGO_BOARD_MAXSIZE			100

#define BINGO_RESULT_BINGO		0x01
#define BINGO_RESULT_NOTHING	0x00
typedef uint8_t bingo_result_t;

typedef struct {
	uint8_t value;
	uint8_t marked;
} bingo_number_t;
typedef struct {
	bingo_number_t numbers[5][5];
	uint8_t number_count;
	bingo_result_t result;
} bingo_board_t;
typedef struct {
	uint8_t	random_number[BINGO_RANDOMNUMBER_MAXSIZE];
	uint32_t random_num_count;
	bingo_board_t boards[BINGO_BOARD_MAXSIZE];
	uint32_t booard_count;
	uint32_t board_winners;
} bingo_t;

bingo_t bingo;

void bingo_init(void) {
	memset(&bingo, 0x00, sizeof(bingo_t));
}

Std_ReturnType bingo_add_row(bingo_board_t *bingo_board, uint8_t *raw) {
	if (bingo_board->number_count >= (5*5)) {
		return E_NOT_OK;
	}

	uint8_t raw_number = bingo_board->number_count / 5;

	bingo_board->numbers[raw_number][0].value = raw[0];
	bingo_board->numbers[raw_number][1].value = raw[1];
	bingo_board->numbers[raw_number][2].value = raw[2];
	bingo_board->numbers[raw_number][3].value = raw[3];
	bingo_board->numbers[raw_number][4].value = raw[4];
	bingo_board->number_count += 5;

	return E_OK;
}

void bingo_print_board(bingo_board_t bingo_board) {
	for (uint8_t i = 0; i < 5; i++) {
		printf("\t");
		for (uint8_t j = 0; j < 5; j++) {
			if (bingo_board.numbers[i][j].marked) {
				printf(" [%02d] ", bingo_board.numbers[i][j].value);
			} else {
				printf("  %02d  ", bingo_board.numbers[i][j].value);
			}
		}
		printf("\n");
	}
}

void bingo_mark_number(bingo_board_t *bingo_board, uint8_t number) {
	for (uint8_t i = 0; i < 5; i++) {
		for (uint8_t j = 0; j < 5; j++) {
			if (bingo_board->numbers[i][j].value == number) {
				bingo_board->numbers[i][j].marked = 1;
			}
		}
	}
}

uint32_t bingo_sum_nonmarked(bingo_board_t bingo_board) {
	uint32_t sum = 0;

	for (uint8_t i = 0; i < 5; i++) {
		for (uint8_t j = 0; j < 5; j++) {
			if (bingo_board.numbers[i][j].marked != 1) {
				sum += 	bingo_board.numbers[i][j].value;			
			}
		}
	}

	return sum;
}

bingo_result_t bingo_check_row(bingo_board_t *bingo_board) {
	uint8_t bingo_counter = 0;

	for (uint8_t i = 0; i < 5; i++) {
		for (uint8_t j = 0; j < 5; j++) {
			if (bingo_board->numbers[i][j].marked) {
				bingo_counter++;
			}
		}

		if (bingo_counter == 5) {
			printf("BINGO!!! %02d %02d %02d %02d %02d\n", 
				bingo_board->numbers[i][0].value,
				bingo_board->numbers[i][1].value,
				bingo_board->numbers[i][2].value,
				bingo_board->numbers[i][3].value,
				bingo_board->numbers[i][4].value
			);
			return BINGO_RESULT_BINGO;
		}

		bingo_counter = 0;
	}

	return BINGO_RESULT_NOTHING;
}

bingo_result_t bingo_check_column(bingo_board_t *bingo_board) {
	uint8_t bingo_counter = 0;

	for (uint8_t i = 0; i < 5; i++) {
		for (uint8_t j = 0; j < 5; j++) {
			if (bingo_board->numbers[j][i].marked) {
				bingo_counter++;
			}
		}

		if (bingo_counter == 5) {
			printf("BINGO!!! %02d %02d %02d %02d %02d\n", 
				bingo_board->numbers[0][i].value,
				bingo_board->numbers[1][i].value,
				bingo_board->numbers[2][i].value,
				bingo_board->numbers[3][i].value,
				bingo_board->numbers[4][i].value
			);
			return BINGO_RESULT_BINGO;
		}

		bingo_counter = 0;
	}

	return BINGO_RESULT_NOTHING;
}

void bingo_perform_draw(void) {
	bingo_result_t row_result = BINGO_RESULT_NOTHING;
	bingo_result_t col_result = BINGO_RESULT_NOTHING;
	uint32_t sum_nonmarked = 0;
	uint32_t final_score = 0;

	printf("Bingo stat: random num count: %d\n", bingo.random_num_count);
	for (uint8_t draw_idx = 0; draw_idx < bingo.random_num_count; draw_idx++) {
		printf("Bing Draw #%02d - Number: %d\n", draw_idx, bingo.random_number[draw_idx]);
		for (uint8_t board_idx = 0; board_idx < bingo.booard_count; board_idx++) {
			if (bingo.boards[board_idx].result == BINGO_RESULT_NOTHING) {
				bingo_mark_number(&bingo.boards[board_idx], bingo.random_number[draw_idx]);
				printf("Bingo board: %d\n", board_idx);
				bingo_print_board(bingo.boards[board_idx]);
				printf("\n");

				row_result = bingo_check_row(&bingo.boards[board_idx]);
				col_result = bingo_check_column(&bingo.boards[board_idx]);

				if (row_result == BINGO_RESULT_BINGO || col_result == BINGO_RESULT_BINGO) {

					bingo.boards[board_idx].result = BINGO_RESULT_BINGO;
					bingo.board_winners++;

					if (bingo.board_winners == bingo.booard_count) {
						sum_nonmarked = bingo_sum_nonmarked(bingo.boards[board_idx]);
						final_score = sum_nonmarked * bingo.random_number[draw_idx];

						printf("Final score: %d (sum_nonmarked: %d, number: %d)\n",
							final_score, sum_nonmarked, bingo.random_number[draw_idx]
						);
						return;
					}
				}
			}
		}
	}
}

void process_line(char *line, int length, int line_number) {
	char *token = NULL;
	char *end = NULL;
	static uint8_t board_parsing_state = 0;
	uint8_t raw[5] = {0,0,0,0,0};
	uint8_t raw_idx = 0;

	//printf("line #%d: %s", line_number, line);
	if (line_number == 0) {
		token = strtok(line, ",");
		while (token != NULL) {
    		bingo.random_number[bingo.random_num_count++] = strtol(token, &end, 10);
    		token = strtok(NULL, ",");
		}

		printf("Random numbers (%d):\n", bingo.random_num_count);
		for (uint32_t i = 0; i < bingo.random_num_count; i++) {
			if (i == bingo.random_num_count-1) {
				printf("%d\n", bingo.random_number[i]);
			} else {
				printf("%d, ", bingo.random_number[i]);
			}
		}
	} else {
		switch (board_parsing_state) {
			case 0:
				// Process the starting delimiter
				if (strcmp(line, "\n") == 0) {
					board_parsing_state = 1;
					printf("Start parsing board: %d:\n", bingo.booard_count);
				}
				break;
			case 1:
				token = strtok(line, " ");
				while (token != NULL) {
    				raw[raw_idx++] = strtol(token, &end, 10);
    				token = strtok(NULL, " ");
				}

				bingo_add_row(&bingo.boards[bingo.booard_count], raw);
			
				if (bingo.boards[bingo.booard_count].number_count >= (5*5)) {
					bingo_print_board(bingo.boards[bingo.booard_count]);
					bingo.booard_count++;
					board_parsing_state = 0;
					printf("End of parsing\n");
				}
				break;
			default:
				printf("ERROR: Invalid board parsing state: %d\n", board_parsing_state);
				break;
		}
	}	
}

int main(int argc, char **argv) {
    char *filename = "inputs/2021_04_input.txt";

    char linebuffer[LINEBUFFER_MAX_LENGTH];
    int line_number = 0;

    FILE *fp = fopen(filename, "r");

    if (fp == NULL)
    {
        printf("Error: could not open file %s", filename);
        return 1;
    }

    while (fgets(linebuffer, sizeof(linebuffer), fp)) {
    	process_line(linebuffer, strlen(linebuffer), line_number);
		line_number++;
    }

	bingo_perform_draw();


    // close the file
    fclose(fp);
	return 0;
}


