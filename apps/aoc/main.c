#include <common/Std_Types.h>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

static void main_processcommandline(int argc, char **argv) {
	int c;

	while ((c = getopt(argc, argv, "hvc:")) != -1) {
		switch (c) {
			case '?': // Unknown option
			case 'h':
				printf("usage:\n");
				printf("       -h         - this help\n");
				printf("       -v         - version\n");
				printf("       -c [file]  - use given config file\n");
				exit(0);
			case 'v':
				printf("Version\n");
				exit(0);
			case 'c':
				printf("Config File:%s\n", optarg);
				break;
			default:
				exit(1);
		}
	}
}



int main(int argc, char **argv) {
    main_processcommandline(argc, argv);

    printf("Hello World");

	while (1) {
		
	}
}
