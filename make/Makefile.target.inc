
CC := gcc

CFLAGS += -g 
CFLAGS += -Wall 
CFLAGS += -std=gnu99
CFLAGS += -O4 
CFLAGS += -funsigned-bitfields 
CFLAGS += -funsigned-char 
CFLAGS += -Wno-unused-result

ifeq ($(LOG), 1)
CFLAGS += -DLOG_LEVEL=$(LOG)
endif