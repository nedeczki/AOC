# Project DIRs

ROOT_DIR := $(abspath $(dir $(lastword $(MAKEFILE_LIST)))..)
APPS_DIR := $(ROOT_DIR)/apps
LIBS_DIR := $(ROOT_DIR)/libs

PROJECT_NAME := $(shell basename `git rev-parse --show-toplevel`)
COMMON_LIB := $(ROOT_DIR)/libs/common

INCLUDES += -I$(COMMON_LIB)


#dirs:
#	@echo ROOT_DIR := $(ROOT_DIR)
#	@echo APPS_DIR := $(APPS_DIR)
#	@echo LIBS_DIR := $(LIBS_DIR)
