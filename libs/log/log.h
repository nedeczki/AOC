/**
 ********************************************************************************
 * @file    log.h
 * @author  nedeczki
 * @date    2022-04-07
 * @brief   
 ********************************************************************************
 */

#ifndef LOG_H
#define LOG_H

#include <stdint.h>

#define LOG_LEVEL_INFO					0U
#define LOG_LEVEL_DEBUG					1U

typedef uint8_t log_level_t;

#ifndef LOG_LEVEL
#define LOG_LEVEL                       LOG_LEVEL_INFO
#endif

#define LOG_CFG_PRINT_BUFFER_MAXSIZE	256

#define LOG(fmt, ...) log_print(LOG_LEVEL_INFO, fmt, ##__VA_ARGS__);
#define LOG_DBG(fmt, ...) log_print(LOG_LEVEL_DEBUG, fmt, ##__VA_ARGS__);

#if LOG_LEVEL != LOG_LEVEL_DEBUG
#define LOG_DBG_START   (log_level_current = LOG_LEVEL_DBG)
#define LOG_DBG_END     (log_level_current = LOG_LEVEL_INFO)
#else
#define LOG_DBG_START
#define LOG_DBG_END
#endif

extern log_level_t log_level_current;
void log_print(log_level_t log_level,  const char *format, ...);

#endif /* LOG_H */
