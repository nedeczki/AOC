/**
 ********************************************************************************
 * @file    log.c
 * @author  nedeczki
 * @date    2022-04-07
 * @brief   
 ********************************************************************************
 */

#include "log.h"

#include <stdio.h>
#include <stdarg.h>

log_level_t log_level_current = LOG_LEVEL;

void log_print(log_level_t log_level,  const char *format, ...) {
	if (log_level > log_level_current)
		return;

	char buffer[LOG_CFG_PRINT_BUFFER_MAXSIZE];

	va_list args;
	va_start(args, format);
	vsnprintf(buffer, LOG_CFG_PRINT_BUFFER_MAXSIZE, format, args);
	va_end(args);
    printf("%s", buffer);
}
