#ifndef COMMON_H
#define COMMON_H

#include <Std_Types.h>

#ifndef min
#define min(a,b) (((a) < (b)) ? (a) : (b))
#endif
#ifndef max
#define max(a,b) (((a) > (b)) ? (a) : (b))
#endif

#define absdiff(a,b) (max((a),(b)) - min((a),(b)))

#endif /* COMMON_H */