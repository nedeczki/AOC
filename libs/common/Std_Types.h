// Std_Types.h is based on AUTOSAR: XXX (link)

#ifndef STD_TYPES_H
#define STD_TYPES_H

#include <stdint.h>

typedef uint8_t Std_ReturnType;
#define E_OK        0x00u
#define E_NOT_OK    0x01u

#define STD_HIGH    0x01u
#define STD_LOW     0x00u

#define STD_ACTIVE  0x01u
#define STD_IDLE    0x00u

#define STD_ON      0x01u
#define STD_OFF     0x00u

#endif /* STD_TYPES_H */
