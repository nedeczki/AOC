#ifndef BINNUM_H
#define BINNUM_H

#include <common/Std_Types.h>
#include <stdio.h>

inline uint8_t binnum_get_bit_u16(const uint16_t num, const uint8_t pos) {
	uint8_t bit;

	bit = (num & (uint16_t)(1 << pos)) >> pos;

	return bit;
}

void binnum_print_u16_number(uint16_t num, uint8_t lenght);

#endif /* BINNUM_H */
